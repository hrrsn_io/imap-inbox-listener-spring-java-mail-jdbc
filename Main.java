package com.nolessan;

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IdleManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.event.MessageCountAdapter;
import javax.mail.event.MessageCountEvent;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by hgm12 on 8/26/2015.
 */
@Component
public class Main {

    static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String args[]) {

        ApplicationContext context =
                new ClassPathXmlApplicationContext("Spring-Context.xml");
        System.out.println("in main");

        final DaoImpl dao = (DaoImpl) context.getBean("daoImpl");

        IMAPFolder folder;
        Store store;

        ExecutorService es = Executors.newCachedThreadPool();

        try {
            Properties props = System.getProperties();
            props.setProperty("mail.store.protocol", "imaps");
            props.setProperty("mail.imaps.usesocketchannels", "true");

            Session session = Session.getDefaultInstance(props, null);

            final IdleManager idleManager = new IdleManager(session, es);


            store = session.getStore("imaps");
            store.connect("hostname", "email@email.com", "password");

            folder = (IMAPFolder) store.getFolder("INBOX");


            if (!folder.isOpen())
                folder.open(Folder.READ_WRITE);
            //idleManager.watch(folder);
            final Message[] messages = folder.getMessages();
            System.out.println("No of Messages : " + folder.getMessageCount());
            System.out.println("No of Unread Messages : " + folder.getUnreadMessageCount());
            System.out.println(messages.length);

            folder.addMessageCountListener(new MessageCountAdapter() {
                public void messagesAdded(MessageCountEvent ev) {
                    Folder folder = (Folder) ev.getSource();
                    Message[] msgs = ev.getMessages();
                    System.out.println("Folder: " + folder +
                            " got " + msgs.length + " new messages");
                    String phoneNumber = null;
                    try {
                        for (Message i : msgs) {
                            Multipart multipart = (Multipart) i.getContent();
                            BodyPart bodyPart = multipart.getBodyPart(0);
                            String messageContent = bodyPart.getContent().toString();
                            System.out.println(messageContent);
                            if (StringUtils.containsIgnoreCase(messageContent, "stop")) {
                                Address[] froms = i.getFrom();
                                String email = froms == null ? null : ((InternetAddress) froms[0]).getAddress();
                                System.out.println(email);
                                phoneNumber = email.substring(0, 10);
                                System.out.println(phoneNumber);
                                dao.updateFlag(phoneNumber, "y");
                                logger.info(phoneNumber + " indicated stop");
                            } else if (StringUtils.containsIgnoreCase(messageContent, "continue")) {
                                Address[] froms = i.getFrom();
                                String email = froms == null ? null : ((InternetAddress) froms[0]).getAddress();
                                System.out.println(email);
                                phoneNumber = email.substring(0, 10);
                                System.out.println(phoneNumber);
                                dao.updateFlag(phoneNumber, "n");
                                logger.info(phoneNumber + " indicated continue");
                            }
                        }

                    } catch (MessagingException e) {
                        logger.error(e);
                    } catch (IOException e) {
                        logger.error(e);
                    }
                    try {
                        // process new messages
                        idleManager.watch(folder); // keep watching for new messages
                    } catch (MessagingException mex) {
                        logger.error(mex);
                    }
                }
            });
            idleManager.watch(folder);

        } catch (NoSuchProviderException e) {
            logger.error(e);
        } catch (MessagingException e) {
            logger.error(e);
        } catch (IOException e) {
            logger.error(e);
        } 

    }
}