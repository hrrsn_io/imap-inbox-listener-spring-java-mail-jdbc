package com.nolessan;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by hgm12 on 8/26/2015.
 */
@Repository("daoImpl")
public class DaoImpl {

    @Autowired
    JdbcTemplate jdbcTemplate;

    static Logger logger = Logger.getLogger(DaoImpl.class.getName());

    public List<Map<String, Object>> selectQuery() {
        List<Map<String, Object>> resultList = jdbcTemplate.queryForList("select phone_number from Carrier");
        return resultList;
    }

    public void updateFlag(String from, String stopValue){
        try {
            jdbcTemplate.update("update Carrier set stop=? where phone_number=?", stopValue, from);
        } catch (DataAccessException e) {
            logger.error(e);
        }

    }


}